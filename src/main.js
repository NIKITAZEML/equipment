import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from "axios";
import Paginate from 'vuejs-paginate'
Vue.config.productionTip = false

Vue.component('paginate', Paginate)

new Vue({
  store,
  router,
  axios,
  render: h => h(App)
}).$mount('#app')
