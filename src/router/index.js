import Vue from 'vue'
import VueRouter from 'vue-router'
import AddNote from "../components/adding data/AddNote";
import EditData from "../components/editind data/EditNote";
import GetAllEquipment from "../components/get all data/GetAllEquipment";
import GetAllTypes from "../components/get all data/GetAllTypes";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Main',
        component: AddNote
    },
    {
        path: '/edit-data',
        name: 'Edit',
        component:  EditData
    },
    {
        path: '/get-all-equipment',
        name: 'GetAll',
        component:  GetAllEquipment
    },
    {
        path: '/get-all-equipment-types',
        name: 'GetAll',
        component:  GetAllTypes
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
